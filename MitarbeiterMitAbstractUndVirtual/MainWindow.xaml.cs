using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace MitarbeiterMitAbstractUndVirtual
{
    public class MainWindow : Window
    {
        private List<CMitarbeiter> _mitarbeiters;
        
        private TextBox _inputName;
        private TextBox _inputVorname;
        private TextBox _inputGehalt;
        private TextBox _inputVornameArbeiter;
        private TextBox _inputNameArbeiter;
        private TextBox _inputStunden;
        private TextBox _inputStundenLohn;
        private TextBox _mitarbeiterTextBox;
        private TabControl _tabControl;

        public MainWindow()
        {
            _mitarbeiters = new List<CMitarbeiter>();
            
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            _inputName = this.FindControl<TextBox>("InputName");
            _inputVorname = this.FindControl<TextBox>("InputVorname");
            _inputGehalt = this.FindControl<TextBox>("InputGehalt");
            _inputVornameArbeiter = this.FindControl<TextBox>("InputVornameArbeiter");
            _inputNameArbeiter = this.FindControl<TextBox>("InputNameArbeiter");
            _inputStunden = this.FindControl<TextBox>("InputStunden");
            _inputStundenLohn = this.FindControl<TextBox>("InputStundenLohn");
            _tabControl = this.FindControl<TabControl>("MitarbeiterTabControl");
            _mitarbeiterTextBox = this.FindControl<TextBox>("MitarbeiterResultTextBox");
        }
        
        private void ResetAngestellterInput()
        {
            _inputName.Text = "";
            _inputVorname.Text = "";
            _inputGehalt.Text = "";
        }
        
        private void ResetArbeiterInput()
        {
            _inputVornameArbeiter.Text = "";
            _inputNameArbeiter.Text = "";
            _inputStunden.Text = "";
            _inputStundenLohn.Text = "";
        }
        
        private async void ShowMessageBox()
        {
            ResetAngestellterInput();
            ResetArbeiterInput();
            await MessageBox.Show(this, "Check Input Boxes and Try Again", "Warning", MessageBox.MessageBoxButtons.Ok);
        }

        private void Add_Button_OnClick(object? sender, RoutedEventArgs e)
        {
            TabItem currentItem = _tabControl.SelectedItem as TabItem;
            String name = currentItem.Name;
            switch (name)
            {
                case "AngestelltenTabItem":
                    try
                    {
                        if(_inputName.Text.Length > 0 && _inputVorname.Text.Length > 0 && _inputGehalt.Text.Length > 0)
                        {
                            _mitarbeiters.Add(new CAngestellte(_inputName.Text, _inputVorname.Text, Convert.ToDouble(_inputGehalt.Text)));
                            ResetAngestellterInput();
                        }
                        else
                        {
                            ShowMessageBox();
                        }
                    }
                    catch (Exception)
                    {
                        ShowMessageBox();
                        ResetAngestellterInput();
                    }
                    break;
                case "ArbeiterTabItem":
                    try
                    {
                        if (_inputNameArbeiter.Text.Length > 0 && _inputVornameArbeiter.Text.Length > 0 
                                && _inputStunden.Text.Length > 0 && _inputStundenLohn.Text.Length > 0)
                        {
                            _mitarbeiters.Add(new CArbeiter(_inputNameArbeiter.Text, _inputVornameArbeiter.Text, Convert.ToInt32(_inputStunden.Text), Convert.ToDouble(_inputStundenLohn.Text)));
                            ResetArbeiterInput();
                        }
                        else
                        {
                            ShowMessageBox();
                        }
                    }
                    catch (Exception)
                    {
                        ShowMessageBox();
                        ResetAngestellterInput();
                    }
                    break;
                default:
                    ShowMessageBox();
                    break;
            }
        }

        private void Show_All_Button_OnClick(object? sender, RoutedEventArgs e)
        {
            String text = "";
            foreach (var mitarbeiter in _mitarbeiters)
            {
                text += mitarbeiter.Ausgabe() + "\n";
            }
            _mitarbeiterTextBox.Text = text;
        }
    }
}
