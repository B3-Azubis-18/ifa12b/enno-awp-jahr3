﻿using Avalonia;
using Avalonia.Logging.Serilog;

namespace MitarbeiterMitAbstractUndVirtual
{
    abstract class CMitarbeiter
    {
        private string nachname;
        private string vorname;

        public CMitarbeiter(string nN, string vN)
        {
            nachname = nN;
            vorname = vN;
        }
        public string GetNachname()
        {
            return nachname;
        }
        public string GetVorname()
        {
            return vorname;
        }
        private void SetNachname(string nN)
        {
            nachname = nN;
        }
        private void SetVorname(string vN)
        {
            vorname = vN;
        }
        public virtual string Ausgabe() 
        { 
            return $"{GetVorname()} : {GetNachname()}";
        }
    }
    class CAngestellte : CMitarbeiter
    {
        private double bruttogehalt = 1.0;
        public CAngestellte(string nN, string vN, double brutto):base(nN,vN)
        {
            SetBrutto(brutto);
        }
        public bool SetBrutto(double brutto)
        {
            if (brutto > 0)
            {
                bruttogehalt = brutto;
                return true;
            }

            return false;
        }
        public double GetBrutto()
        {
            return bruttogehalt;
        }
        public override string Ausgabe()
        {
            return base.Ausgabe() + GetBrutto();
        }
    }
    class CArbeiter : CMitarbeiter
    {
        private int stunden;
        private double stundenlohn = 1.0;
        public CArbeiter(string nN, string vN, int std, double stdLohn):base(nN,vN)
        {
            SetStunden(std);
            SetStundenlohn(stdLohn);
        }
        public double GetBrutto()
        {
            return (stunden * stundenlohn);
        }
        public bool SetStunden(int std)
        {
            if (std >= 0)
            {
                stunden = std;
                return true;
            }

            return false;
        }
        public bool SetStundenlohn(double stdLohn)
        {
            if (stdLohn > 0.0)
            {
                stundenlohn = stdLohn;
                return true;
            }

            return false;
        }
        public override string Ausgabe()
        {
            return base.Ausgabe() + GetBrutto();
        }
    }

    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args) => BuildAvaloniaApp()
            .StartWithClassicDesktopLifetime(args);

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug();
    }
}