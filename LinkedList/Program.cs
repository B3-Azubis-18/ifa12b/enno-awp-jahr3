﻿using System;
using System.Collections.Generic;

namespace LinkedList
{
    class Program
    {
        private static CPupil _head;
        private static Dictionary<int, string> _menu = new Dictionary<int, string>();
        
        static void Main(string[] args)
        {
            CreateMenuOptions();
            int chosenOption = -1;
            do
            {
                ConsolenMenue();
                chosenOption = AskOption();
                if (chosenOption == 0)
                {
                    break;
                }
                DoWork(chosenOption);
            } while (chosenOption > 0 && chosenOption <= _menu.Count);
            Console.WriteLine("Bye!");
        }

        private static void CreateMenuOptions()
        {
            _menu.Add(1, "Datensaetze ansehen");
            _menu.Add(2, "Neuen Datensatz am Ende anfuegen");
            _menu.Add(3, "Neuen Datensatz vor Element Nr. x einfuegen");
            _menu.Add(4, "Datensatz loeschen");
            _menu.Add(5, "Komplette Liste loeschen");
            _menu.Add(6, "Liste nach Name sortieren");
            _menu.Add(7, "Liste in Datei speichern");
            _menu.Add(8, "Liste aus Datei lesen");
            _menu.Add(9, "Bildschirm loeschen");
            _menu.Add(0, "Programm beenden");
        }

        private static void ConsolenMenue()
        {
            Console.WriteLine("Auswahlmenue Verkettete Liste");
            Console.WriteLine("=============================");
            foreach (var menu_item in _menu)
            {
                Console.WriteLine("{0}: {1}", menu_item.Key, menu_item.Value);
            }
        }

        private static int AskOption()
        {
            int intAntwort = -1;
            Console.WriteLine("Bitte die gewuenschte Antwort eingeben:");
            string antwort = Console.ReadLine();
            try
            {
                intAntwort = Convert.ToInt32(antwort);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            if (intAntwort >= 0)
            {
                return intAntwort;
            }
            else
            {
                Console.WriteLine("Falsche Option gewaehlt! Bitte erneut waehlen!");
                return AskOption();
            }
        }

        private static void DoWork(int chosenOption)
        {
            switch (chosenOption)
            {
                case 1:
                    ShowList(_head);
                    break;
                case 2:
                    AddEnd(ref _head);
                    break;
                case 3:
                    AddBeforeElement(ref _head);
                    break;
                case 4:
                    DeleteElement();
                    break;
                case 5:
                    DeleteList();
                    break;
                case 6:
                    // TODO: Sort by name
                    break;
                case 7:
                    // TODO: Export to file
                    break;
                case 8:
                    // TODO: Import from file
                    break;
                case 9:
                    Console.Clear();
                    break;
                default:
                    Console.WriteLine("Unbekannte oder nicht implementierte Option gewaehlt!");
                    break;
            }
        }

        static void ShowList(CPupil anchor)
        {
            Console.WriteLine("############ List begin ############");
            if (_head == null)
            {
                Console.WriteLine("No Elements in the List");
                Console.WriteLine("############ List end   ############");
                return;
            }
            CPupil tmpCPupil = _head;
            do
            {
                Console.WriteLine("Element: " + tmpCPupil.ToString());
                tmpCPupil = tmpCPupil.Next;
            } while (tmpCPupil != null);
            Console.WriteLine("############ List end   ############");
        }

        static CPupil AddEnd(ref CPupil anchor)
        {
            CPupil newCPupil = new CPupil();
            CPupil tmpCPupil = _head;
            if (tmpCPupil == null)
            {
                _head = newCPupil;
            }
            else
            {
                while (tmpCPupil.Next != null)
                {
                    tmpCPupil = tmpCPupil.Next;
                }
                tmpCPupil.Next = newCPupil;
            }
            return newCPupil;
        }

        static CPupil AddBeforeElement(ref CPupil anchor)
        {
            return null;
        }

        static void DeleteElement()
        {
            // TODO
        }

        static void DeleteList()
        {
            // TODO
        }

        static CPupil SearchByNr()
        {
            return null;
        }
    }

    internal class CPupil
    {
        public int iNr;
        public string strName;
        public string strLocation;
        public string strPhone;
        public CPupil Next = null;

        public override string ToString()
        {
            return String.Format("{0} {1} {2} {3}", iNr, strName, strLocation, strPhone);
        }
    }
}