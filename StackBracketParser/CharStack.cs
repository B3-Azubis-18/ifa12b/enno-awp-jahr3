namespace StackBracketParser
{
    public class CharStack
    {
        private readonly int _iMaxAnzahl;

        private int _count;

        public int Count
        {
            get => _count;
            private set
            {
                if (value >= 0 && value <= _iMaxAnzahl)
                {
                    _count = value;
                }
            }
        }

        private readonly char[] _cWerte;
        
        public CharStack(int size)
        {
            _count = 0;
            _iMaxAnzahl = size;
            _cWerte = new char[Count];
        }

        public bool Push(char character)
        {
            if (Count >= _iMaxAnzahl) return false;
            _cWerte[Count] = character;
            Count++;
            return true;

        }

        public char Pop()
        {
            if (IsEmpty()) return ' ';
            Count--;
            return _cWerte[Count];

        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public char Top()
        {
            return _cWerte[_iMaxAnzahl - 1];
        }
    }
}