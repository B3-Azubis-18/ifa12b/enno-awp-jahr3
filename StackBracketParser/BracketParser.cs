using System;

namespace StackBracketParser
{
    public class BracketParser
    {
        private CharStack _original;
        private int _stackSize;
        
        public BracketParser()
        {
            _original = new CharStack(0);
            _stackSize = 0;
        }

        public void AddCharacter(char c)
        {
            _stackSize++;
            CharStack newStack = new CharStack(_stackSize);
            while (!_original.IsEmpty())
            {
                newStack.Push(_original.Pop());
            }

            newStack.Push(c);
        }
        
        public void AddCharacters(char[] c)
        {
            foreach (var element in c)
            {
                AddCharacter(element);
            }
        }
        
        public void InsertCharacterAt(char c, int position)
        {
            if (position - 1 > _stackSize)
            {
                throw new ArgumentOutOfRangeException();
            }

            _stackSize++;
            CharStack newStack = new CharStack(_stackSize);

            for (int currentPostion = 0; currentPostion < position; currentPostion++)
            {
                newStack.Push(_original.Pop());
            }
            newStack.Push(c);
            while (!_original.IsEmpty())
            {
                newStack.Push(_original.Pop());
            }
        }
        
        public char RemoveCharacterAt(int position)
        {
            // Do stuff
            return ' ';
        }
        
        public CharStack RemoveCharacterEnd(int numberOfCharacters)
        {
            CharStack removedChars = new CharStack(numberOfCharacters);
            for (int i = 0; i < numberOfCharacters; i++)
            {
                removedChars.Push(_original.Pop());
            }
            return removedChars;
        }

        public char RemoveCharacter()
        {
            return _original.Pop();
        }

        public bool IsValid()
        {
            return ErrorCount() == 0;
        }

        public int ErrorCount()
        {
            return Parse(new [] {' '}, 0, new CharStack(5));
        }

        private CharStack DropUnimportantCharacters()
        {
            CharStack cleanStack = new CharStack(0);
            CharStack originalCopy = _original;
            int newStackSize = 0;
            while (!originalCopy.IsEmpty())
            {
                char c = originalCopy.Pop();
                if (c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}')
                {
                    newStackSize++;
                    CharStack newCleanStack = new CharStack(newStackSize);
                    while (!cleanStack.IsEmpty())
                    {
                        newCleanStack.Push(cleanStack.Pop());
                    }
                    newCleanStack.Push(c);
                    cleanStack = newCleanStack;
                }
            }
            return cleanStack;
        }
        
        private int Parse(char[] buf, int start, CharStack s) {
            int mistakes = 0;
            for(int i = start; buf[i] != '\0'; i++) {
                char c = buf[i];
                if (isOpening(c))
                    s.Push(c);
                else {
                    if(s.IsEmpty())
                        mistakes++;
                    else if(s.Top() == invert(c))
                        s.Pop();
                    else {
                        int m1 = Parse(buf, i+1, s);
                        s.Pop();
                        int m2 = Parse(buf, i, s);
                        if(m1 < m2)
                            mistakes += m1;
                        else
                            mistakes += m2;
                        return mistakes + 1;
                    }
                }
            }
	
            mistakes += s.Count;
            return mistakes;
        }

        private bool isOpening(char c)
        {
            return c == '(' || c == '[' || c == '{';
        }

        private char invert(char c)
        {
            switch (c)
            {
                case '(':
                    return ')';
                case  ')':
                    return '(';
                case '[':
                    return ']';
                case  ']':
                    return '[';
                case '{':
                    return '}';
                case '}':
                    return '{';
                default:
                    throw new ArgumentException();
            }
        }
    }
}
