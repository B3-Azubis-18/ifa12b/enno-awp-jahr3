namespace CRechteck
{
    public class CRechteck
    {
        // Default init is 0 for doubles
        private double dA;
        private double dB;

        public CRechteck(double A, double B)
        {
            setSeitenlaengen(A, B);
        }

        public override string ToString()
        {
            return dA + "x" + dB;
        }

        public void setSeitenlaengen(double A, double B)
        {
            if (A > 0)
            {
                dA = A;
            }

            if (B > 0)
            {
                dB = B;
            }
        }

        public double getA()
        {
            return dA;
        }

        public double getB()
        {
            return dB;
        }

        public double getFlaeche()
        {
            return dA * dB;
        }

        public double getUmfang()
        {
            return 2 * (dA + dB);
        }
    }
}
