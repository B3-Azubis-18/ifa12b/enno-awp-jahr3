﻿using System;
using System.Collections.Generic;

namespace CRechteck
{
    class Program
    {
        static List<CRechteck> rechtecke = new List<CRechteck>();

        static void Main(string[] args)
        {
            int aktion = -1;
            do
            {
                Menu();
                aktion = AktionEinlesen();

                switch (aktion)
                {
                    case 1:
                        AddRechteck();
                        break;
                    case 2:
                        RechteckeAusgeben();
                        break;
                    case 3:
                        LoescheRechteck();
                        break;
                    case 4:
                        ListeLeeren();
                        break;
                    default:
                        Console.WriteLine("Unbekannte Option.");
                        break;
                }
            } while (aktion > 0);
        }

        static void Menu()
        {
            Console.WriteLine("1: Neues Rechteck erstellen");
            Console.WriteLine("2: Rechtecke ausgeben");
            Console.WriteLine("3: Rechteck loeschen (nur nach ID)");
            Console.WriteLine("4: Liste mit Rechtecken leeren");
        }

        static int AktionEinlesen()
        {
            Console.WriteLine("Bitte geben sie die gewuenschte Aktion ein:");
            String action = Console.ReadLine();
            int returnValue = -1;
            try
            {
                returnValue = Convert.ToInt32(action);
            }
            catch
            {
                // Do nothing
            }

            if (returnValue > 0 && returnValue <= 4)
            {
                return returnValue;
            }
            else
            {
                return -1;
            }
        }

        static void AddRechteck()
        {
            rechtecke.Add(new CRechteck(0,0));
        }

        static void RechteckeAusgeben()
        {
            // TODO
        }

        static void LoescheRechteck()
        {
            // TODO
        }

        static void ListeLeeren()
        {
            rechtecke = new List<CRechteck>();
        }
    }
}
